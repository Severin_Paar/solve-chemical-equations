<!DOCTYPE html>
<html lang="en">

<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta http-equiv="X-UA-Compatible" content="ie=edge">
	<title>Solve chemical equations</title>

	<style>
		* {
			font-family: Calibri;
			font-size: 1.03em;
		}
	</style>
</head>

<body>
	<p>
		<span>This Project is using complex AI* and Machine Learning* techniques, accelerated with Blockchain*,<br> in order to calculate the most accurate results.</span><br>
		<span>Because of that, only easy equations can be solved.</span>
		<br>
		<br>
		<span>Formatting is important!</span>
		<br>
		<br>
		<span>Eg:</span><br>
		<span>H2+O2=H2*O</span><br>
		<span>Fe+Cl2=Fe*Cl3</span><br>
		<br>
		<span>Too long (Warning - your browser will hang for several seconds):</span><br>
		<span>K*Mn*O4+H*Cl=K*Cl+Mn*Cl2+H2*O+Cl2</span>
		<br>
		<br>
		<br>
		<span>Not supported:</span>
		<ul>
		<li>Brackets: <span style="color:red">(</span>H<sub>2</sub><span style="color:red">)</span><sub><sub>2</sub></sub></li>
		<li>Half-solved equations: <span style="color:red">2</span>H<sub>2</sub>+O<sub>2</sub>=H<sub>2</sub>*O</li>
		</ul>
	</p>
	<br>

	<input id="input" name="input" type="text">
	<button onclick="solve()">Solve</button>
	<br>
	<span id="output"></span>

	<br><br><br><br><br><br><br><br><br>
	<span>* Bruteforce, Bruteforce, and Bruteforce</span>


	<script src="projekte/chemical_equations/solve.js"></script>
	<script src="projekte/chemical_equations/script.js"></script>

	<!-- for developing at home -->
	<script src="solve.js"></script>
	<script src="script.js"></script>
</body>

</html>