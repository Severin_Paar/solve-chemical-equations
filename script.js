function solve() {
	try {
		result = solveEquation(document.querySelector("#input").value);
		document.querySelector("#output").innerHTML = parse4Humans(result);
	} catch(e) {
		document.querySelector("#output").innerHTML = `<span style="color:red">No result found.<br>Either the equation takes too long to calculate, or the equation has no result.</span>`
	}
}

function parse4Humans(equation) {

	/*
	Function to parse an object to an equation (eg: H2*O=H3+O2).
	*/

	let data = "";

	for (let side = 0; side <= 1; side++) {
		equation[side].keys().forEach(el => {
			data += "<span style='color:red'>" + equation[side][el]["multiplier"] + "</span>";
			equation[side][el].keys().forEach((element, i) => {
				if (i < equation[side][el].keys().length - 1)
					data += equation[side][el][element]["element"] + "<sub>" + equation[side][el][element]["multiplier"] + "</sub>" + "+";
			});
			data += "*";
		});
		data += "=";
	}
	data = data.replace(/\+\*/g, "*").replace("*=", "=").replace("*=", "").replace(/1/g, "");

	return data;
}