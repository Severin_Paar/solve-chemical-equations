function solveEquation(equation) {
	equation = parse(equation)

	let depth = 5;
	let result = {};
	const time_start = Date.now();
	while(time_start > Date.now() - 5000) { // If no result was found after 5 seconds, stop
		result = bruteForceEquation(equation, depth);

		if(!result) {
			depth++;
			console.log(depth);
		} else {
			break;
		}
	}
	return result;
}

function parse(equation) {

	/*
	Function to parse an equation (eg: H2*O=H3+O2) to an object.
	*/

	equation = equation.replace(/ /g, "");

	let data = {};
	data["elements"] = [];

	// split in left and right side of equation
	equation.split("=").forEach((side, index) => {
		data[index] = {};

		// split in seperate parts
		side.split("+").forEach((el, p) => {
			data[index][p] = { "multiplier": 1 };

			// split in seperate elements
			el.split("*").forEach((element, i) => {
				data[index][p][i] = {};

				// get multiplier from element (eg H2 -> 2)
				multiplier = getMultiplierOfElement(element);
				element = element.replace(multiplier, "");

				data[index][p][i]["multiplier"] = multiplier;
				data[index][p][i]["element"] = element;

				data["elements"].add(element);
			});
		});
	});

	return data;
}


function bruteForceEquation(equation, depth) {

	/*
	Function to try every possible combination,and return if the right one is found.
	*/

	const keysL = equation[0].keys().length;
	const keys = equation[0].keys().length + equation[1].keys().length;

	combinations = getCombinations(keys, depth);

	for (el = 0; el < combinations.length; el++) {
		for (i = 0; i < combinations[el].length; i++) {
			side = i >= keysL ? 1 : 0;
			index = i >= keysL ? i - keysL : i;
			equation[side][index]["multiplier"] = combinations[el][i];
		};
		if (checkEquation(equation)) return equation;
	};
}


function getCombinations(length, depth) {
	// works somehow. copied from SO.
	let signs = [];
	for (let i = 1; i <= depth; i++) {
		signs.push(i);
	}
	const cartesian = array => array.reduce((a, b) => a.reduce((r, v) => r.concat(b.map(w => [].concat(v, w))), []));
	return cartesian(Array.from({ length }, _ => signs));
}


function checkEquation(equation) {

	/*
	Function to check if an Equation is true.
	Only works with parsed equations (=Objects)
	*/

	let occurrencesL = {};
	let occurrencesR = {};
	let leftSide = equation[0];
	let rightSide = equation[1];

	equation["elements"].forEach(element => {
		occurrencesL[element] = 0;
		occurrencesR[element] = 0;
	});


	leftSide.keys().forEach(key => {
		let multiplier = leftSide[key]["multiplier"];
		for (let i = 0; i < leftSide[key].keys().length - 1; i++) {
			occurrencesL[leftSide[key][i]["element"]] += leftSide[key][i]["multiplier"] * multiplier;
		}
	});
	rightSide.keys().forEach(key => {
		let multiplier = rightSide[key]["multiplier"];
		for (let i = 0; i < rightSide[key].keys().length - 1; i++) {
			occurrencesR[rightSide[key][i]["element"]] += rightSide[key][i]["multiplier"] * multiplier;
		}
	});

	return occurrencesEqual(occurrencesL, occurrencesR);
}



function getMultiplierOfElement(element) {
	try {
		return element.match(/\d+/g).map(Number)[element.match(/\d+/g).map(Number).length - 1];
	} catch (e) {
		return 1;
	}
}


// adds an element to the array if it does not already exist
Array.prototype.add = function (element) {
	if (this.indexOf(element) === -1) this.push(element);
};

// get the keys of an object
Object.prototype.keys = function () {
	return Object.keys(this);
};

// check if occurrences equal
function occurrencesEqual(object1, object2) {
	let equal = true;
	object1.keys().forEach(i => {
		if (object1[i] !== object2[i]) equal = false;
	});
	return equal;
}